// JWT - standard for sending info between app in a secure manner; will allow access to methods that will help in creating a JSON web token
const jwt = require("jsonwebtoken")
const secret = "HindiKoAlam"

// jwt is a way of securely passing info from the server to the frontend or other parts of the server
// info is kept secure through the use of the secret
// only the system that knows the secret code can decode the encrypted info.

//******TOKEN CREATION**********

module.exports.createAccessToken=(user)=>{
	// data will be received from the registration form. when the users login, a token will be created with the users' information
	const data={
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	// generates the token using the form data and the secret code without additional options provided.
	return jwt.sign(data, secret,{})

}

// Token Verification

module.exports.verify=(req, res, next)=>{
	let token = req.headers.authorization;
	if (typeof token !== "undefined") {
		console.log(token);
		token = token.slice(7, token.length); //to remove the "bearer" in the token

		//verification of the token (verify method)
		return jwt.verify(token, secret, (err,data) =>{
			if (err) {
				return res.send({auth:"failed"})
			}else{
				//next-allows the app to proceed to the next callback function in the route
				next();
			}
		})
	}else{
		return res.send({auth:"failed"})
	}
}

// Token decryption
module.exports.decode=(token)=>{
	// if token is received and not undefined
	if (token !== "undefined") {
		token=token.slice(7, token.length);
		return jwt.verify(token,secret,(err,data)=>{
			if (err) {
				return null
			}else{
				// decode - used to obtain the information from the jwt
				// complete:true - an option that allows returning additional information from the JWT
				// payload - contains the information that is stored when the token is generated
				return jwt.decode(token, {complete:true}).payload;
			} 
		})
	}else{
		return null;
	}
}