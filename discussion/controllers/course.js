const Course = require("../models/Course")
const User = require("../models/User")

module.exports.addCourse = (reqBody, userData) => {

    return Course.findById(userData.userId).then(result => {

        if (userData.isAdmin==false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
        
    });    
}


// Retrieve all courses
module.exports.getAllCourses=()=>{
	return Course.find({}).then(result => {
		return result
	})
}

//Retrieve active courses
module.exports.getCourse=()=>{
		return Course.find({isActive:true}).then(result=>{
			return result
	})
}


//Retrieve a course
module.exports.getCourse=(reqParams)=>{
		return Course.findById(reqParams.courseId).then(result=>{
			return result
	})
}

// update a course
module.exports.updatedCourse=(reqParams, reqBody)=>{
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	} 

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error)=>{
			if (error) {
				return false
			}else{
				return true
			}
	})
}

// archiving a course
module.exports.archivedCourse=(reqParams, reqBody)=>{
	let updatedCourse = {
		isActive: reqBody.isActive
	} 

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error)=>{
			if (error) {
				return false
			}else{
				return true
			}
	})
}


