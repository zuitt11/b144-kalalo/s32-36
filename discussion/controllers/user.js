const User = require("../models/User")
const Course = require("../models/Course")
const bcrypt = require('bcrypt');
const auth = require("../auth")

/* checking if the email exists
1. find the duplicate email/s
2. error handling, if no duplicate found, return false, else, return true


IMPORTANT NOTE: best practice to return a  result is to use a boolean or return an object/array of object since string is limited in the backend, and cannot be connected to the frontend.
*/

module.exports.checkEmailExists =(reqBody)=>{
	return User.find({email:reqBody.email}).then(result =>{
		if (result.length>0) {
			return true;
		}else{
			return false;
		}
	})
}


/*User Registration
1. creata a new User object using the mongoose model and the info from the requestBody
2. make sure that the pw is encrypted
3. save the new user to the db
*/
module.exports.registerUser=(reqBody)=>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age: reqBody.age,
		gender: reqBody.gender,
		email: reqBody.email,
		//10 is the value provided as the number of "salt" rounds (algorythm to be added) that the bcrypt algorythm will run in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})
	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


// user authenticator
/*
1. search for username in the db
2.if it does not exist, return false
3.if it exists, check if the pw is correct
4.  return jwt if the user is successfully logged in, false if not
*/
module.exports.loginUser=(reqBody)=>{
	return User.findOne( {email:reqBody.email} ).then(result =>{
			// user does not exist
		if (result==null){
			return false;
		}else{
			// checking pw (decrypting through bcrypt)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { accessToken: auth.createAccessToken(result.toObject()) }//toObject converts mongoose object into plain js object
		}
		else{
			return false;
			}
		}
	})
}



// *******ACTIVITY***********
/*
1. find the id
2. if it does not exist, return false
3. if it exists retrieve the details without the password
*/
module.exports.getProfile =(data)=>{
	return User.findById(data.userId).then(result =>{
		result.password = "";
		return result;
	})
}


// enrolling a student in a course
/*
1. Find the document in the database using the userId
2. Add the courseId to the user's enrollment array
3. Update the document in the database
*/

module.exports.enrol=async(data)=>{
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// adding courseId in the user's enrollment array
		user.enrollment.push({courseId:data.courseId})

		// saving updated information to the database
		return user.save().then((user, err)=>{
			if (err) {
				return false
			}else{
				return true
			}
		})
	})

	// adding userId to the enrollees array of the course
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// adding userId in the course enrollees array
		course.enrollees.push({userId: data.userId})
		// saving to database
		return course.save().then((course, error)=>{
			if(error){
				return false
			}else{
				return true
			}
		})
	})

	// checking if the user and course are updated
	if (isUserUpdated && isCourseUpdated){
		return true
	}else{
		return false
	}
}