// set up  the dependencies

const express = require("express")
const mongoose = require ("mongoose")
const cors = require("cors"); //allows the app's Cross Origin Resource Sharing settings

// connecting to routes needed
const userRoutes = require("./routes/user")
const courseRoutes = require("./routes/course")

// server

const app = express()
const port = 4000
app.use(cors()); //allows all origin to access the backend application;enable all CORS
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// defines the "/api/users" string to be included for all routes defined in the "user" route file
app.use("/api/users", userRoutes)
app.use("/api/courses", courseRoutes)


mongoose.connect("mongodb+srv://marcokalalo:admin@wdc028-course-booking.vyczd.mongodb.net/batch144_booking_system?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology:true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open",()=> console.log(`Now connected to MongoDB Atlas`))





app.listen(port, ()=> console.log(`API now online at port ${port}`));