const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course")
const auth = require("../auth")

// // route for course creation
// router.post("/", (req,res)=>{
// 	courseController.addCourse(req.body).then(result=>res.send(result))
// })


//ACTIVITY s34******************************************
router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    courseController.addCourse(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})
// retrieve all courses
router.get("/all", (req,res)=>{
	courseController.getAllCourses().then(result =>res.send(result))
})

// retrieve all active courses
router.get("/", (req,res)=>{
	courseController.getAllActive().then(result =>res.send(result))
})

// retrieve a specific course
router.get("/:courseId", (req,res)=>{
	console.log(req.params.courseId)
	courseController.getCourse(req.params).then(result =>res.send(result))
})

// update a course
router.put("/:courseId", auth.verify, (req,res)=>{
	// const userData = auth.decode(req.headers.authorization)
	courseController.updatedCourse(req.params, req.body).then(result =>res.send(result))
})

// archiving a course
router.put("/:courseId/archive", auth.verify, (req,res)=>{
	courseController.archivedCourse(req.params, req.body).then(result =>res.send(result))
})




module.exports=router;