const express = require("express");
const router = express.Router();

const auth = require("../auth")
const userController = require("../controllers/user")

//routes for checking if the user's email already exists in the database
router.post("/checkEmail", (req,res)=>{
	userController.checkEmailExists(req.body).then(result => res.send(result))
})


// User registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(result=>res.send(result));
})



// authenticating a user
router.post('/login',(req,res)=>{
	userController.loginUser(req.body).then(result => res.send(result));
})


// *******ACTIVITY***********
// retrieving details of a user
router.get('/details', auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId:userData.id}).then(result => res.send(result));

})

// route to enroll a user in a course
router.post('/enrol', auth.verify, (req, res) =>{
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}
	userController.enrol(data).then(result => res.send(result))
})





module.exports=router;